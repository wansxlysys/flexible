(function() {
	/**
	 *    基础配置
	 *    var flexible = new Flexible({
	 *      // 基准大小
	 *      baseSize: 100,
	 *      // 最大调节宽度
	 *      maxWidth: 540,
	 *      // 设计稿宽度
	 *      designWidth: 375,
	 *      // 隐藏属性名 (页面初始化完成前隐藏)
	 *      hideAttr: "l-hide",
	 *  });
	 **/
	var Flexible = function(options) {

		options = options || {};

		this.baseSize = options.baseSize || 100;
		// 最大调节宽度
		this.maxWidth = options.maxWidth || 540;
		// 设计稿宽度
		this.designWidth = options.designWidth || 375;
		// 隐藏属性名
		this.hideAttr = options.hideAttr || "l-hide";
		// 获取html根标签
		this.element = document.documentElement;

		var that = this;

		// 监听浏览器调整大小
		window.addEventListener("resize", function(event) {
			that.refresh();
		});

		// 监听浏览器后退
		window.addEventListener("pageshow", function(event) {
			if (event.persisted) {
				that.refresh();
			}
		});

		// 初始化根标签字体大小
		this.refresh();

		// 清除隐藏属性
		document.querySelector("[" + this.hideAttr + "]") && document.querySelector("[" + this.hideAttr + "]").removeAttribute(this.hideAttr);
	};

	/**
	 * 刷新比例
	 */
	Flexible.prototype.refresh = function() {
		var clientWidth = this.element.clientWidth;

		if (clientWidth > this.maxWidth) {
			clientWidth = this.maxWidth;
		}

		this.element.style.fontSize = (clientWidth * this.baseSize / this.designWidth) + "px";
	};

	window.Flexible = Flexible;

})(window);
