# 优化后的flexible

1. 将viewprot标签放置head `<meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1,user-scalable=no">`
2. 引入flexble.js (建议放置在head优先调用)

> 调用示例

```
var flexible = new Flexible({
	// 基准大小 1rem = 100px (方便换算)
	baseSize: 100,
	// 最大调节宽度 (超出此宽度不在调节根标签字体大小)
	maxWidth: 750,
	// 设计稿宽度   
	designWidth: 375,
	// 隐藏属性名 (页面初始化完成前隐藏)
	hideAttr: "l-hide",
});
```